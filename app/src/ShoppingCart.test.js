import { ShoppingCart } from './ShoppingCart';

describe('test ShoppingCart math', () => {

    test('cart can add an item', () => {
        const cart = new ShoppingCart();
        cart.add({ objectID: 1 });
        expect(cart.getSummary().count === 1)
    });

    test('cart can add 2 of the same items', () => {
        const cart = new ShoppingCart();
        cart.add({ objectID: 1 });
        cart.add({ objectID: 1 });
        expect(cart.getSummary().count === 2);
        expect(cart.getLineItems().length === 1);
    });

    test('cart can add 2 different items', () => {
        const cart = new ShoppingCart();
        cart.add({ objectID: 1 });
        cart.add({ objectID: 2 });
        expect(cart.getSummary().count === 2);
        expect(cart.getLineItems().length === 2);
    });

    test('cart can add 2 different items with quantities', () => {
        const cart = new ShoppingCart();
        cart.add({ objectID: 1 }, 2);
        cart.add({ objectID: 2 }, 3);
        expect(cart.getSummary().count === 5);
        expect(cart.getLineItems().length === 2);
    });

    test('cart can add 2 different items with quantities and add 2 more', () => {
        const cart = new ShoppingCart();
        cart.add({ objectID: 1 }, 2);
        cart.add({ objectID: 2 }, 3);
        cart.add({ objectID: 1 }, 2);
        expect(cart.getSummary().count === 7);
        expect(cart.getLineItems().length === 2);
    });

    test('cart can add 2 different items with quantities and add a third', () => {
        const cart = new ShoppingCart();
        cart.add({ objectID: 1 }, 2);
        cart.add({ objectID: 2 }, 3);
        cart.add({ objectID: 3 }, 2);
        expect(cart.getSummary().count === 7);
        expect(cart.getLineItems().length === 3);
    });

    test('cart can add up prices', () => {
        const cart = new ShoppingCart();
        cart.add({ objectID: 1, price: 3 }, 2);
        cart.add({ objectID: 2, price: 4 }, 3);
        cart.add({ objectID: 3, price: 5 }, 2);
        expect(cart.getSummary().cost === 28);
    });

    test('cart can delete an item', () => {
        const cart = new ShoppingCart();
        cart.add({ objectID: 1, price: 3 }, 2);
        expect(cart.getLineItems().length === 1);
        expect(cart.getSummary().count === 1);
        cart.delete({ objectID: 1 });
        expect(cart.getLineItems().length === 0);
        expect(cart.getSummary().count === 0);
        expect(cart.getSummary().cost === 0);
    });

    test('cart can updateQuantity', () => {
        const cart = new ShoppingCart();
        cart.add({ objectID: 1, price: 3 }, 2);
        expect(cart.getLineItems().length === 1);
        expect(cart.getSummary().count === 2);
        cart.updateQuantity({ objectID: 1 }, 100);
        expect(cart.getLineItems().length === 1);
        expect(cart.getSummary().count === 100);
        expect(cart.getSummary().cost === 300);
    });

    test('cart can delete an item when quantity is 0', () => {
        const cart = new ShoppingCart();
        cart.add({ objectID: 1, price: 3 }, 2);
        expect(cart.getLineItems().length === 1);
        expect(cart.getSummary().count === 2);
        cart.updateQuantity({ objectID: 1 }, 0);
        expect(cart.getLineItems().length === 0);
        expect(cart.getSummary().count === 0);
        expect(cart.getSummary().cost === 0);
    });

});

