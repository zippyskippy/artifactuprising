import { useEffect } from 'react';

/**
 * EventManager - simplifies events 
 */
export class EventManager {
  constructor(node) {
    this.node = node || this.node || window;
    this.cache = {};  // old events
  }

  //returns the last time this event was dispatched - even prior to your "listener"
  get(event) {
    return this.cache[event];
  }

  echo(event) {
    this.dispatch(this.get(event));
  }

  addListener(event, handler, useCapture) {
    if (!event) return;

    handler =
      handler ||
      function (e) {
        if (!e) return null;
        return e.details || null;
      };

    this.node.addEventListener(event, handler, useCapture);
  }

  removeListener(event, handler) {
    if (!event) return;
    this.node.removeEventListener(event, handler);
  }

  dispatch(event, params, quiet) {
    if (!event) return;
    if (!event.type) {
      let e = event;
      let n = e.event || e.name || e;
      let p = e.params || e.data || params;
      if (typeof p === 'object') {
        p = Object.assign({}, e.params, e.data, params);
      }
      event = new CustomEvent(n, { detail: p });
    }

    this.node.dispatchEvent(event);
    this.cache[event.type] = event;
    try {
      if (!quiet) console.log(`dispatch ${event.type}(${event.detail ? JSON.stringify(event.detail) : ''})`);
    } catch (ex) { }
  }
}

export let events = new EventManager();


/**
 * Dispatch component
 */
export function Dispatch(props) {
  var expr = props.if || props.expr;
  expr = expr == null ? true : expr;
  var wait = props.wait || props.delay || 0;
  useEffect(() => {
    if (expr && props.event) {
      setTimeout(function () {
        events.dispatch(props.event, props.params);
      }, wait);
    }
  });
  return props.children || null;
}

export function EventHandler(props) {

  let complete = (props.event && props.handler);
  if (!complete) {
    throw (new SyntaxError('<EventHandler> is missing required properties.  <EventHandler event="event-name"  handler={func}  />'));
  }

  //manage events
  useEffect(() => {
    //componentDidMount, componentDidChange
    if (complete) events.addListener(props.event, props.handler, props.useCapture);
    return () => {
      //componentWillUnmount
      events.removeListener(props.event, props.handler);
    };
  });

  return props.children || null;
}


export default events;

