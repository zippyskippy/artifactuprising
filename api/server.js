'use strict';

const express = require('express');
const fileSystem = require('fs');

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

// App
const app = express();
app.get('/', (req, res) => {
    var readStream = fileSystem.createReadStream('./data.json');
    readStream.pipe(res);
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);